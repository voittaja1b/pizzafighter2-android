package com.favasben.pizzafighter2;


import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.ui.activity.BaseGameActivity;

import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.WindowManager;



public class Main extends BaseGameActivity{
	
	
 	//------------------------------M�TODO AL CREAR LA ACTIVIDAD		
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);    	    	
    	System.loadLibrary("gdx");		
    	System.gc();
    	
		OpenFeintSettings settingsOF = new OpenFeintSettings("Pizza Fighter 2", "bv9gWmS6R7t7F9OwGv9wA", "Ca3YmeTB0WytlBz8qd1K2S7el1VUXwylD8yuF61X4", "382133");		 
		OpenFeint.initializeWithoutLoggingIn(this, settingsOF, new OpenFeintDelegate() { });		 		 
	}	
 	
 	
 	//-----------------------------AL CARGAR EL MOTOR
    private Camera mCamera; 
 	
	 public Engine onLoadEngine() {	 		
 		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		G.width = displayMetrics.widthPixels;
		G.height = displayMetrics.heightPixels; 
		
		if(G.height>G.width){
			G.width = displayMetrics.heightPixels;
			G.height = displayMetrics.widthPixels; 
		}
		
		G.scaleX = G.width/480f;
		G.scaleY = G.height/320f;
		
		if(displayMetrics.densityDpi==DisplayMetrics.DENSITY_HIGH)
			G.hdpi = true;
		
		SharedPreferences settings = getSharedPreferences("PIZZA_FIGHTER_2", 0);
    	G.volume = (byte) settings.getInt("volume", 1);
    	G.backgroundNum = (byte) settings.getInt("backgroundNum", 0);
    	G.trailNum = (byte) settings.getInt("trailNum", 0);
    	
 		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
 		mCamera = new Camera(0, 0, G.getX(480), G.getY(320));
		 
		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy((int)G.getX(480), (int)G.getY(320)), mCamera).setNeedsMusic(true).setNeedsSound(true);		 
		Engine mEngine = new Engine(engineOptions);
		
		return mEngine;		
	 } 	
 	

	//---------------------------------------AL CARGAR LOS RECURSOS
 	private int P512;
 	private Scene mScene;
 	
 	//capa 0
 	private Texture mSliceTx, mLogoTx;
 	private Sprite mSliceSp, mLogoSp;
 	
 	private Sound mSound;
	
	public void onLoadResources() {			
		mScene = new Scene(1);
		
		if(G.hdpi){
			TextureRegionFactory.setAssetBasePath("hdpi/");
			P512 = 1024;
		}
		else{
			TextureRegionFactory.setAssetBasePath("mdpi/");
			P512 = 512;
		}	
				
		mSliceTx = new Texture(P512, P512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
		TextureRegion mSliceRg = TextureRegionFactory.createFromAsset(mSliceTx, getApplicationContext(), "slice.png", 0, 0);
		mSliceSp = new Sprite(0, 0, 0, G.getY(320), mSliceRg);		
		mScene.getLayer(0).addEntity(mSliceSp);
		mEngine.getTextureManager().loadTextures(mSliceTx);	
		
		mLogoTx = new Texture(P512, P512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 		
		TextureRegion mLogoRg = TextureRegionFactory.createFromAsset(mLogoTx, this, "logo.png",0,0);
		mLogoSp = new Sprite(0, 0, G.getX(480), 0, mLogoRg);
		mScene.getLayer(0).addEntity(mLogoSp);
		mEngine.getTextureManager().loadTextures(mLogoTx);					
		
		try {			
			mSound = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "sounds/newsword.ogg");			
			mSound.setVolume(G.volume);					
		} 
		catch (Exception e) {}
	}
	
	
	//---------------------------------------AL CARGAR LA ESCENA
	private int wSlice=0, hLogo=0;
	private byte wait=45;
	private boolean incSlice=true, incLogo=true, ini=true;
	
	public Scene onLoadScene() {			
		final Handler handler = new Handler();
		Runnable r = new Runnable(){
		    public void run(){
		    	if(mSliceTx.isLoadedToHardware() && mLogoTx.isLoadedToHardware()){
					mEngine.setScene(mScene);
					mEngine.registerUpdateHandler(handleMain);
					
					handler.removeCallbacks(this);
				 }
				 else							
					handler.postDelayed(this, 1000);
		    }
		};
		handler.postDelayed(r, 1000);
		
		return mScene;
	}
	private TimerHandler handleMain = new TimerHandler(1/30f, true, new ITimerCallback(){
		public void onTimePassed(final TimerHandler pTimerHandler) {			
			if(wait>0){
				wait--;
				return;
			}
			
			if(ini){
				ini = false;
				mSound.play();
			}
			
			if(incSlice && wSlice<G.getX(480)){				
				wSlice+=G.getX(80);										
				mSliceSp.setWidth(wSlice);
				
				if(wSlice>=G.getX(480)){
					wait = 20;
					incSlice = false;
				}
			}
			else{
				if(incLogo){
					if(hLogo<G.getY(320)){
						hLogo+=G.getY(8);
						mLogoSp.setHeight(hLogo);
						mLogoSp.setPosition(0, (G.getY(320)-hLogo)/2);
						
						if(hLogo>=G.getY(320)){
							wait = 60;
							incLogo = false;
							mScene.getLayer(0).removeEntity(mSliceSp);
						}
					}				
				}
				else{
					hLogo-=G.getY(8);
					mLogoSp.setHeight(hLogo);
					mLogoSp.setPosition(0, (G.getY(320)-hLogo)/2);
					
					if(hLogo<0){						
						startActivity(new Intent(Main.this, Menu.class));
						finish();	
					}
				}
			}
		}
	});
	

	//---------------------------------------AL TERMINAR DE CARGAR
	
	public void onLoadComplete() {}
	
	
	 //---------------------------AL CERRAR	 
	 
	 public void onDestroy() {		 		 		 		 		 
		 super.onDestroy();	
	     mEngine.getScene().clearChildScene();	        
	     mEngine.getScene().clearTouchAreas();
	     mEngine.getScene().clearUpdateHandlers();	     
		 		
		 System.gc();		 		 
	 }	 
	 
	 
	 //---------------------------AL PRESIONAR BACK	 
	 
	 public void onBackPressed() {
		 System.exit(0);
	 }
}