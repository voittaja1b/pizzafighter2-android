package com.favasben.pizzafighter2;

import android.widget.RelativeLayout;


public final class G {
	public static boolean hdpi = false;		
	public static float width = 100f;
	public static float height = 100f;
	public static float scaleX = 1.0f;
	public static float scaleY = 1.0f;
	
	public static byte volume = 1;	
	public static byte backgroundNum = 3;
	public static byte trailNum = 0;
		
	public static float getX(float x){
		return scaleX*x;
	}
	
	public static float getY(float y){
		return scaleY*y;
	}	
	
	public static RelativeLayout.LayoutParams setParams(float w, float h, float l, float t){
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int)getX(w), (int)getY(h));
		params.setMargins((int)getX(l), (int)getY(t), 0, 0);
		return params;
	}
}

