package com.favasben.pizzafighter2;

import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.khronos.opengles.GL10;

import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.particle.ParticleSystem;
import org.anddev.andengine.entity.particle.emitter.PointParticleEmitter;
import org.anddev.andengine.entity.particle.modifier.ExpireModifier;
import org.anddev.andengine.entity.particle.modifier.ScaleModifier;
import org.anddev.andengine.entity.particle.modifier.VelocityInitializer;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.Scene.IOnAreaTouchListener;
import org.anddev.andengine.entity.scene.Scene.ITouchArea;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.sprite.TiledSprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.WindowManager;



public class Options extends BaseGameActivity implements IOnAreaTouchListener{
	
	
 	//------------------------------M�TODO AL CREAR LA ACTIVIDAD	
	private SharedPreferences settings;
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);    	
    	overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);  
    	System.gc();      	    	
	}	
 	
 	
 	//-----------------------------AL CARGAR EL MOTOR
    private Camera mCamera; 
 	
	 public Engine onLoadEngine() {
 		settings = getSharedPreferences("PIZZA_FIGHTER_2", 0);
 		
 		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mCamera = new Camera(0, 0, G.getX(480), G.getY(320));
		 
		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(G.getX(480), G.getY(320)), mCamera).setNeedsMusic(true).setNeedsSound(true);		 
		Engine mEngine = new Engine(engineOptions);		 

		return mEngine;
	 } 	
 	

	//---------------------------------------AL CARGAR LOS RECURSOS
 	private int P16, P32, P64, P128, P256, P512;
 	private Scene mScene;
 	
 	//capa 0
 	private ParticleSystem mParticlesPS[];
 	
 	//capa 1
 	private Sprite mBgdsSp[], mSwordSp[];
 	private TiledSprite mBackSp, mVolumeSp; 	
 	
 	//audio
 	private Music mMusic=null;
 	private Sound mSounds[];
	
	public void onLoadResources() {			
		mScene = new Scene(2);
		
		if(G.hdpi){
			TextureRegionFactory.setAssetBasePath("hdpi/");
			P16 = 32;
			P32 = 64;
			P64 = 128;
			P128 = 256;
			P256 = 512;
			P512 = 1024;
		}
		else{
			TextureRegionFactory.setAssetBasePath("mdpi/");
			P16 = 16;
			P32 = 32;
			P64 = 64;
			P128 = 128;
			P256 = 256;
			P512 = 512;
		}	
				
		Texture mBgdsTx[] = new Texture[4];		
		TextureRegion mBgdsRg[] = new TextureRegion[4];		
		mBgdsSp = new Sprite[4];
		for(byte i=0; i<4; i++){
			mBgdsTx[i] = new Texture(P512, P512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			mBgdsRg[i] = TextureRegionFactory.createFromAsset(mBgdsTx[i], this, "back"+i+".png",0,0);			
			mBgdsSp[i] = new Sprite(0, 0, G.getX(200), G.getY(120), mBgdsRg[i]);
			mBgdsSp[i].setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
			mScene.getLayer(1).addEntity(mBgdsSp[i]);
			mBgdsSp[i].setAlpha(0.4f);
		}		
		mBgdsSp[0].setPosition(G.getX(25), G.getY(10));
		mBgdsSp[1].setPosition(G.getX(255), G.getY(10));		
		mBgdsSp[2].setPosition(G.getX(25), G.getY(140));
		mBgdsSp[3].setPosition(G.getX(255), G.getY(140));
		mBgdsSp[G.backgroundNum].setAlpha(1f);
		mEngine.getTextureManager().loadTextures(mBgdsTx);	
		
		Texture mParticlesTx[] = new Texture[4];
		TextureRegion mParticlesRg[] = new TextureRegion[4]; 
		PointParticleEmitter mParticlesPE[] = new PointParticleEmitter[4];
		mParticlesPS = new ParticleSystem[4];
		for(byte i=0; i<4; i++){
			mParticlesTx[i] = new Texture(P16, P16, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			mParticlesRg[i] = TextureRegionFactory.createFromAsset(mParticlesTx[i], this, "spark"+i+".png", 0, 0);
			mParticlesPE[i] = new PointParticleEmitter(G.getX(192+i*66),G.getY(278));
			mParticlesPS[i] = new ParticleSystem(mParticlesPE[i], 30, 100, 500, mParticlesRg[i]);
			mParticlesPS[i].addParticleInitializer(new VelocityInitializer(G.getX(-80), G.getX(80), G.getY(-80), G.getY(80)));			
			mParticlesPS[i].addParticleModifier(new ScaleModifier(1f, 0f, 0f, 0.7f));	
			mParticlesPS[i].addParticleModifier(new ExpireModifier(0.7f));
		}
		mEngine.getTextureManager().loadTextures(mParticlesTx);
		
		Texture mSwordTx[] = new Texture[4];		
		TextureRegion mSwordRg[] = new TextureRegion[4];		
		mSwordSp = new Sprite[4];
		for(byte i=0; i<4; i++){
			mSwordTx[i] = new Texture(P32, P32, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			mSwordRg[i] = TextureRegionFactory.createFromAsset(mSwordTx[i], this, "point"+i+".png",0,0);			
			mSwordSp[i] = new Sprite(G.getX(176+i*66), G.getY(275), G.getX(40), G.getY(40), mSwordRg[i]);
			mSwordSp[i].setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
			mScene.getLayer(1).addEntity(mSwordSp[i]);
			mSwordSp[i].setAlpha(0.4f);
		}		
		mSwordSp[G.trailNum].setAlpha(1f);
		updateParticles();
		mEngine.getTextureManager().loadTextures(mSwordTx);	
		
		Texture mVolumeTx = new Texture(P128, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
		TiledTextureRegion mVolumeRg = TextureRegionFactory.createTiledFromAsset(mVolumeTx, getApplicationContext(), "sound.png", 0, 0, 2, 1);
		mVolumeSp = new TiledSprite(G.getX(440), G.getY(275), G.getX(40), G.getY(40), mVolumeRg);		
		mScene.getLayer(1).addEntity(mVolumeSp);
		mVolumeSp.setCurrentTileIndex(G.volume);
		mEngine.getTextureManager().loadTextures(mVolumeTx);
		
		Texture mBackTx = new Texture(P256, P256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
		TiledTextureRegion mBackRg = TextureRegionFactory.createTiledFromAsset(mBackTx, getApplicationContext(), "back.png", 0, 0, 1, 3);
		mBackSp = new TiledSprite(0, G.getY(270), G.getX(150), G.getY(50), mBackRg);
		mBackSp.setRotationCenter(G.getX(75), G.getY(25));
		mScene.getLayer(1).addEntity(mBackSp);
		mEngine.getTextureManager().loadTextures(mBackTx);
		
	//SONIDOS
		try {			
			mMusic = MusicFactory.createMusicFromAsset(mEngine.getMusicManager(), this, "sounds/menu.ogg");				
			mMusic.setLooping(true);				
			mMusic.setVolume(G.volume*0.6f);
			mMusic.play();	
			
			mSounds = new Sound[2];
			mSounds[0] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "sounds/buttonmenu.ogg");			
			mSounds[1] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "sounds/newsword.ogg");			
			for(byte i=0; i<mSounds.length; i++)
				mSounds[i].setVolume(G.volume);
		} catch (Exception e) {}
	}
	
	
	//---------------------------------------AL CARGAR LA ESCENA
	private byte buttonId=0, toCButton=3;
	
	public Scene onLoadScene() {
		mScene.getLayer(1).registerTouchArea(mBackSp);
		mScene.getLayer(1).registerTouchArea(mBgdsSp[0]);
		mScene.getLayer(1).registerTouchArea(mBgdsSp[1]);
		mScene.getLayer(1).registerTouchArea(mBgdsSp[2]);
		mScene.getLayer(1).registerTouchArea(mBgdsSp[3]);
		mScene.getLayer(1).registerTouchArea(mSwordSp[0]);
		mScene.getLayer(1).registerTouchArea(mSwordSp[1]);
		mScene.getLayer(1).registerTouchArea(mSwordSp[2]);
		mScene.getLayer(1).registerTouchArea(mSwordSp[3]);
		mScene.getLayer(1).registerTouchArea(mVolumeSp);
		mScene.setOnAreaTouchListener(this);		
		mEngine.registerUpdateHandler(handleMenu);
		
		return mScene;
	}
	private TimerHandler handleMenu = new TimerHandler(1/30f, true, new ITimerCallback(){
		public void onTimePassed(final TimerHandler pTimerHandler) {				    	
	    	if(--toCButton==0){	    			    		
				toCButton = 3;
				if(++buttonId==3)
					buttonId = 0;
				
				mBackSp.setCurrentTileIndex(buttonId);
			}	    		    	
		}
	});
	

	//---------------------------------------AL HACER TOUCH EN UN BOT�N	
	SharedPreferences.Editor editor;	
	
	public boolean onAreaTouched(TouchEvent pEvent,ITouchArea pArea, float pTouchAreaLocalX,float pTouchAreaLocalY) {
		if(pEvent.getAction()==TouchEvent.ACTION_DOWN){							
			if(pArea.equals(mBackSp)){				
				mBackSp.addShapeModifier(new org.anddev.andengine.entity.shape.modifier.ColorModifier(0.1f, 0f, 1.5f, 0f, 1.5f, 0f, 1.5f));
				mSounds[0].play();
				iniDelay();
			}
			else if(pArea.equals(mBgdsSp[0])){
				for(byte i=0; i<4; i++)
					mBgdsSp[i].setAlpha(0.4f);
				mBgdsSp[0].setAlpha(1f);
				mSounds[1].play();
				
				editor = settings.edit();
				editor.putInt("backgroundNum", 0);		
				editor.commit();
				G.backgroundNum = 0;
			}
			else if(pArea.equals(mBgdsSp[1])){
				for(byte i=0; i<4; i++)
					mBgdsSp[i].setAlpha(0.4f);
				mBgdsSp[1].setAlpha(1f);
				mSounds[1].play();
				
				editor = settings.edit();
				editor.putInt("backgroundNum", 1);		
				editor.commit();
				G.backgroundNum = 1;
			}
			else if(pArea.equals(mBgdsSp[2])){
				for(byte i=0; i<4; i++)
					mBgdsSp[i].setAlpha(0.4f);
				mBgdsSp[2].setAlpha(1f);
				mSounds[1].play();
				
				editor = settings.edit();
				editor.putInt("backgroundNum", 2);		
				editor.commit();
				G.backgroundNum = 2;
			}
			else if(pArea.equals(mBgdsSp[3])){
				for(byte i=0; i<4; i++)
					mBgdsSp[i].setAlpha(0.4f);
				mBgdsSp[3].setAlpha(1f);
				mSounds[1].play();
				
				editor = settings.edit();
				editor.putInt("backgroundNum", 3);		
				editor.commit();
				G.backgroundNum = 3;
			}
			else if(pArea.equals(mSwordSp[0])){
				for(byte i=0; i<4; i++)
					mSwordSp[i].setAlpha(0.4f);
				mSwordSp[0].setAlpha(1f);
				mSounds[1].play();
				
				editor = settings.edit();
				editor.putInt("trailNum", 0);		
				editor.commit();
				G.trailNum = 0;
				updateParticles();
			}
			else if(pArea.equals(mSwordSp[1])){
				for(byte i=0; i<4; i++)
					mSwordSp[i].setAlpha(0.4f);
				mSwordSp[1].setAlpha(1f);
				mSounds[1].play();

				editor = settings.edit();
				editor.putInt("trailNum", 1);		
				editor.commit();
				G.trailNum = 1;	
				updateParticles();
			}
			else if(pArea.equals(mSwordSp[2])){
				for(byte i=0; i<4; i++)
					mSwordSp[i].setAlpha(0.4f);
				mSwordSp[2].setAlpha(1f);
				mSounds[1].play();
				
				editor = settings.edit();
				editor.putInt("trailNum", 2);		
				editor.commit();
				G.trailNum = 2;
				updateParticles();
			}
			else if(pArea.equals(mSwordSp[3])){
				for(byte i=0; i<4; i++)
					mSwordSp[i].setAlpha(0.4f);
				mSwordSp[3].setAlpha(1f);
				mSounds[1].play();
				
				editor = settings.edit();
				editor.putInt("trailNum", 3);		
				editor.commit();
				G.trailNum = 3;			
				updateParticles();
			}
			else if(pArea.equals(mVolumeSp)){
				editor = settings.edit();
				if(G.volume==1){
					G.volume = 0;					
					mMusic.setVolume(0);
					mSounds[0].setVolume(0);
					mSounds[1].setVolume(0);
				}
				else{
					G.volume = 1;
					mMusic.setVolume(G.volume*0.6f);
					mSounds[0].setVolume(1);
					mSounds[1].setVolume(1);
					mSounds[1].play();										
				}
					
				editor.putInt("volume", G.volume);
				editor.commit();		
				mVolumeSp.setCurrentTileIndex(G.volume);
			}
		}
		
		return true;
	}
	public void updateParticles(){
		for(byte i=0; i<4; i++)
			mScene.getLayer(0).removeEntity(mParticlesPS[i]);
		mScene.getLayer(0).addEntity(mParticlesPS[G.trailNum]);
	}
	public void iniDelay(){
		mScene.getLayer(1).unregisterTouchArea(mBackSp);
		mScene.getLayer(1).unregisterTouchArea(mBgdsSp[0]);
		mScene.getLayer(1).unregisterTouchArea(mBgdsSp[1]);
		mScene.getLayer(1).unregisterTouchArea(mBgdsSp[2]);
		mScene.getLayer(1).unregisterTouchArea(mBgdsSp[3]);
		mScene.getLayer(1).unregisterTouchArea(mSwordSp[0]);
		mScene.getLayer(1).unregisterTouchArea(mSwordSp[1]);
		mScene.getLayer(1).unregisterTouchArea(mSwordSp[2]);
		mScene.getLayer(1).unregisterTouchArea(mSwordSp[3]);
		mScene.getLayer(1).unregisterTouchArea(mVolumeSp);
		
		final Timer timer = new Timer();
		TimerTask timerFunc = new TimerTask()
		{
			
			public void run() {
				timer.cancel();
				startActivity(new Intent(Options.this, Menu.class));
				finish();																														
			}
		};   				
		timer.schedule(timerFunc, 1500);				
	}

	
	//---------------------------------------M�TODO OBTIENE UN # ALEATORIO ENTRE LINF Y LSUP, INCLU�DOS
	public int getRandom(double inf, double sup){
		return (int) Math.round((sup-inf)*Math.random()+inf);
	}	
	
	
	//---------------------------------------AL TERMINAR DE CARGAR
	
	public void onLoadComplete() {}	
	
	
	
	//---------------------------AL PAUSAR	 
	 
	 public void onPause() {
		 super.onPause();
		 if(mMusic!=null)
			 mMusic.pause();
	 }
	 
	 
	 //---------------------------AL REANUDAR	
	 
	 public void onResume() {
		 super.onResume();
		 if(mMusic!=null)
			 mMusic.resume();	
	 }	 
	 
	 
	 //---------------------------AL CERRAR	 
	 
	 public void onDestroy() {		 		 		 		 		 
		 super.onDestroy();	
	     mEngine.getScene().clearChildScene();	        
	     mEngine.getScene().clearTouchAreas();
	     mEngine.getScene().clearUpdateHandlers();	     
		
		 System.gc();		 		 
	 }	 
	 
	 
	 //---------------------------AL PRESIONAR BACK	 
	 
	 public void onBackPressed() {
		 System.exit(0);
	 }
}