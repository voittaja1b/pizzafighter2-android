package com.favasben.pizzafighter2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.khronos.opengles.GL10;

import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.particle.ParticleSystem;
import org.anddev.andengine.entity.particle.emitter.PointParticleEmitter;
import org.anddev.andengine.entity.particle.modifier.AlphaModifier;
import org.anddev.andengine.entity.particle.modifier.ExpireModifier;
import org.anddev.andengine.entity.particle.modifier.RotationModifier;
import org.anddev.andengine.entity.particle.modifier.ScaleModifier;
import org.anddev.andengine.entity.particle.modifier.VelocityInitializer;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.Scene.IOnAreaTouchListener;
import org.anddev.andengine.entity.scene.Scene.IOnSceneTouchListener;
import org.anddev.andengine.entity.scene.Scene.ITouchArea;
import org.anddev.andengine.entity.shape.modifier.ColorModifier;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.sprite.TiledSprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.buffer.BufferObjectManager;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;



public class Arcade extends BaseGameActivity implements IOnSceneTouchListener, IOnAreaTouchListener{	
	
	
 	//---------------------------------------M�TODO AL CREAR LA ACTIVIDAD	
 	private int score=0, best=0;
 	private SharedPreferences settings;
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);    	
    	overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);  
    	System.gc();     	    	
	}	
 	

 	//-----------------------------AL CARGAR EL MOTOR
 	private Camera mCamera;
 	
	 public Engine onLoadEngine() {
 		settings = getSharedPreferences("PIZZA_FIGHTER_2", 0);
    	best = settings.getInt("bestArcade", 0);
    	score = 0;
 		
 		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mCamera = new Camera(0, 0, G.getX(480), G.getY(320));
		 
		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(G.getX(480), G.getY(320)), mCamera).setNeedsMusic(true).setNeedsSound(true);		 
		Engine mEngine = new Engine(engineOptions);
		
		return mEngine;
	 } 	
 	

	//---------------------------------------AL CARGAR LOS RECURSOS
 	private int P8, P16, P32, P64, P128, P256, P512;
 	private Scene mScene;
 	
 	//capa 0
 	private Texture mBackgroundTx;
 	private Sprite mBackgroundSp, mBlurSp[];
 	private TiledSprite mScoreSp[], mBestSp[], mTimeSp[]; 	
 	
 	//capa 1
 	private Sprite mPizzaSp[];
 	private TextureRegion mPRg[], mPxRg, mPxxRg, mPxxxRg, mPxxxxRg, mPxxxxxRg, mPxxxxxxRg;
 	private TiledSprite mBombSp[];
 	private PointParticleEmitter mFirePE;
 	
	//capa 2
	private Sprite mLineSp[], mPointSp[], mOverSp, mTenSp;
	private PointParticleEmitter mSparkPE, mTomatoPE, mChampignonPE, mHamPE;	
	
	//capa 3
 	private Sprite mPlaySp, mPauseSp, mRestartSp, mMenuSp, mInactiveSp;
 	private TiledSprite mComboSp;
 	
 	//audio
 	private Music mMusic=null, mBombon=null;
 	private Sound mSounds[];
	
	public void onLoadResources() {			
		mScene = new Scene(4);
		
		if(G.hdpi){
			TextureRegionFactory.setAssetBasePath("hdpi/");
			P8 = 16;
			P16 = 32;
			P32 = 64;
			P64 = 128;
			P128 = 256;
			P256 = 512;
			P512 = 1024;
		}
		else{
			TextureRegionFactory.setAssetBasePath("mdpi/");
			P8 = 8;
			P16 = 16;
			P32 = 32;
			P64 = 64;
			P128 = 128;
			P256 = 256;
			P512 = 512;
		}	
		
		//CAPA 0
			mBackgroundTx = new Texture(P512, P512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mBackgroundRg = TextureRegionFactory.createFromAsset(mBackgroundTx, this, "back"+G.backgroundNum+".png", 0, 0);
			mBackgroundSp = new Sprite(0, 0, G.getX(480), G.getY(320), mBackgroundRg);	
			mScene.getLayer(0).addEntity(mBackgroundSp);
			mEngine.getTextureManager().loadTextures(mBackgroundTx);
			
			Texture mBlurTx[] = new Texture[12];
			TextureRegion mBlurRg[] = new TextureRegion[12];
			mBlurSp = new Sprite[12];
			for(byte i=0; i<12; i++){
				mBlurTx[i] = new Texture(P128, P32, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
				mBlurRg[i] = TextureRegionFactory.createFromAsset(mBlurTx[i], this, "blur"+(i/2)+".png", 0, 0);
				mBlurSp[i] = new Sprite(-1000, 0, G.getX(32), G.getY(32), mBlurRg[i]);
				mBlurSp[i].setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);				
			}
			mEngine.getTextureManager().loadTextures(mBlurTx);
			
			Texture mScoreBestTx = new Texture(P512, P32, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TiledTextureRegion mScoreBestRg = TextureRegionFactory.createTiledFromAsset(mScoreBestTx, this, "scores.png", 0, 0, 11, 1);			
			mScoreSp = new TiledSprite[5];
			for(byte i=0; i<5; i++){
				mScoreSp[i] = new TiledSprite(G.getX(138+i*18), G.getY(9), G.getX(18), G.getY(25), mScoreBestRg.clone());
				mScoreSp[i].setCurrentTileIndex(0);
				mScene.getLayer(0).addEntity(mScoreSp[i]);
				mScoreSp[i].setVisible(false);
			}			
			mBestSp = new TiledSprite[5];
			for(byte i=0; i<5; i++){
				mBestSp[i] = new TiledSprite(G.getX(69+i*15), G.getY(55), G.getX(15), G.getY(20), mScoreBestRg.clone());
				mBestSp[i].setCurrentTileIndex(0);
				mScene.getLayer(0).addEntity(mBestSp[i]);
				mBestSp[i].setVisible(false);
			}
			mEngine.getTextureManager().loadTexture(mScoreBestTx);
			
			Texture mTimeTx = new Texture(P512, P32, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TiledTextureRegion mTimeRg = TextureRegionFactory.createTiledFromAsset(mTimeTx, this, "times.png", 0, 0, 11, 1);			
			mTimeSp = new TiledSprite[4];
			for(byte i=0; i<4; i++){
				mTimeSp[i] = new TiledSprite(G.getX(365+i*18), G.getY(9), G.getX(18), G.getY(25), mTimeRg.clone());				
				mScene.getLayer(0).addEntity(mTimeSp[i]);				
			}	
			mTimeSp[0].setCurrentTileIndex(1);
			mTimeSp[1].setCurrentTileIndex(10);
			mTimeSp[2].setCurrentTileIndex(0);
			mTimeSp[3].setCurrentTileIndex(0);
			mEngine.getTextureManager().loadTexture(mTimeTx);			
			
		//CAPA 1		 
			Texture[] mPTx = new Texture[24];
			mPRg = new TextureRegion[24];
			for(byte i=0; i<24; i++){
				if(i<6)
					mPTx[i] = new Texture(P128, P128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
				else if(i<12)
					mPTx[i] = new Texture(P64, P128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
				else if(i<18)
					mPTx[i] = new Texture(P64, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
				else
					mPTx[i] = new Texture(P64, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
				
				mPRg[i] = TextureRegionFactory.createFromAsset(mPTx[i], this, "p"+i+".png", 0, 0);		
			}	
			mEngine.getTextureManager().loadTextures(mPTx);
		
			Texture mPxTx = new Texture(P128, P128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
			mPxRg = TextureRegionFactory.createFromAsset(mPxTx, this, "px.png", 0, 0);
			Texture mPxxTx = new Texture(P64, P128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
			mPxxRg = TextureRegionFactory.createFromAsset(mPxxTx, this, "pxx.png", 0, 0);
			Texture mPxxxTx = new Texture(P8, P128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
			mPxxxRg = TextureRegionFactory.createFromAsset(mPxxxTx, this, "pxxx.png", 0, 0);
			Texture mPxxxxTx = new Texture(P64, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
			mPxxxxRg = TextureRegionFactory.createFromAsset(mPxxxxTx, this, "pxxxx.png", 0, 0);
			Texture mPxxxxxTx = new Texture(P64, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
			mPxxxxxRg = TextureRegionFactory.createFromAsset(mPxxxxxTx, this, "pxxxxx.png", 0, 0);
			Texture mPxxxxxxTx = new Texture(P8, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
			mPxxxxxxRg = TextureRegionFactory.createFromAsset(mPxxxxxxTx, this, "pxxxxxx.png", 0, 0);
			mEngine.getTextureManager().loadTextures(mPxTx,mPxxTx,mPxxxTx,mPxxxxTx,mPxxxxxTx,mPxxxxxxTx);
			
			Texture mBombTx = new Texture(P128, P128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TiledTextureRegion mBombRg = TextureRegionFactory.createTiledFromAsset(mBombTx, this, "bomb.png", 0, 0, 3, 1);
			mBombSp = new TiledSprite[4];
	        for(byte i=0; i<4; i++){	        	    
	        	mBombSp[i] = new TiledSprite(0, -1000, G.getX(30), G.getY(90), mBombRg.clone());
	        	mBombSp[i].setScaleCenter(G.getX(15),G.getY(45));
	        	mBombSp[i].setCurrentTileIndex(0);
	        	mScene.getLayer(1).addEntity(mBombSp[i]);		
	        }
			mEngine.getTextureManager().loadTexture(mBombTx);
			
			Texture mFireTx = new Texture(P64, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mFireRg = TextureRegionFactory.createFromAsset(mFireTx, this, "fire.png", 0, 0);
			mFirePE = new PointParticleEmitter(-1000,0);
			ParticleSystem mFirePS = new ParticleSystem(mFirePE, 15, 20, 100, mFireRg);            
			mFirePS.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
			mFirePS.addParticleInitializer(new VelocityInitializer(G.getX(-20), G.getX(20), G.getY(-20), G.getY(20)));			
			mFirePS.addParticleModifier(new RotationModifier(0f, 360f, 0f, 1f));							
			mFirePS.addParticleModifier(new AlphaModifier(1f, 0f, 0f, 1f));
			mFirePS.addParticleModifier(new ExpireModifier(1f));
			mScene.getLayer(1).addEntity(mFirePS);
			mEngine.getTextureManager().loadTexture(mFireTx);
						
		//CAPA 2
			Texture mSparkTx = new Texture(P16, P16, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mSparkRg = TextureRegionFactory.createFromAsset(mSparkTx, this, "spark"+G.trailNum+".png", 0, 0);
			mSparkPE = new PointParticleEmitter(-1000,0);
			ParticleSystem mSparkPS = new ParticleSystem(mSparkPE, 15, 20, 200, mSparkRg);            
			mSparkPS.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
			mSparkPS.addParticleInitializer(new VelocityInitializer(G.getX(-45), G.getX(45), G.getY(-45), G.getY(45)));			
			mSparkPS.addParticleModifier(new RotationModifier(0f, 360f, 0f, 1f));	
			mSparkPS.addParticleModifier(new ScaleModifier(1f, 0f, 0f, 1f));			
			mSparkPS.addParticleModifier(new AlphaModifier(1f, 0f, 0f, 1f));
			mSparkPS.addParticleModifier(new ExpireModifier(1f));
			mScene.getLayer(2).addEntity(mSparkPS);
			mEngine.getTextureManager().loadTexture(mSparkTx);
				
			Texture mLineTx = new Texture(P32, P32, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mLineRg = TextureRegionFactory.createFromAsset(mLineTx, this, "line"+G.trailNum+".png", 0, 0);                
	        mLineSp = new Sprite[20];
	        for(byte i=0; i<20; i++){	        	    
	        	mLineSp[i] = new Sprite(-1000, 0, G.getX(10), G.getY(10), mLineRg.clone());
	        	mLineSp[i].setRotationCenter(0,G.getY(5));			
	        }
	        mEngine.getTextureManager().loadTextures(mLineTx);
        
			Texture mPointTx = new Texture(P32, P32, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mPointRg = TextureRegionFactory.createFromAsset(mPointTx, this, "point"+G.trailNum+".png", 0, 0);                
	        mPointSp = new Sprite[20];
	        for(byte i=0; i<20; i++)
	        	mPointSp[i] = new Sprite(-1000, 0, G.getX(10), G.getY(10), mPointRg.clone());
	        mEngine.getTextureManager().loadTexture(mPointTx);
	        
	        Texture mTomatoTx = new Texture(P16, P16, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mTomatoRg = TextureRegionFactory.createFromAsset(mTomatoTx, this, "tomato.png", 0, 0);
			mTomatoPE = new PointParticleEmitter(-1000,0);
			ParticleSystem mTomatoPS = new ParticleSystem(mTomatoPE, 8, 10, 100, mTomatoRg);  			
			mTomatoPS.addParticleInitializer(new VelocityInitializer(G.getX(-140), G.getX(140), G.getY(-140), G.getY(140)));			
			mTomatoPS.addParticleModifier(new ScaleModifier(1f, 0f, 0f, 0.7f));	
			mTomatoPS.addParticleModifier(new ExpireModifier(0.7f));
			mScene.getLayer(2).addEntity(mTomatoPS);
			mEngine.getTextureManager().loadTexture(mTomatoTx);
		
			Texture mChampignonTx = new Texture(P16, P16, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mChampignonRg = TextureRegionFactory.createFromAsset(mChampignonTx, this, "champignon.png", 0, 0);
			mChampignonPE = new PointParticleEmitter(-1000,0);
			ParticleSystem mChampignonPS = new ParticleSystem(mChampignonPE, 8, 10, 100, mChampignonRg);  			
			mChampignonPS.addParticleInitializer(new VelocityInitializer(G.getX(-140), G.getX(140), G.getY(-140), G.getY(140)));				
			mChampignonPS.addParticleModifier(new ScaleModifier(1f, 0f, 0f, 0.7f));	
			mChampignonPS.addParticleModifier(new ExpireModifier(0.7f));
			mScene.getLayer(2).addEntity(mChampignonPS);
			mEngine.getTextureManager().loadTexture(mChampignonTx);
			
			Texture mHamTx = new Texture(P16, P16, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mHamRg = TextureRegionFactory.createFromAsset(mHamTx, this, "ham.png", 0, 0);
			mHamPE = new PointParticleEmitter(-1000,0);
			ParticleSystem mHamPS = new ParticleSystem(mHamPE, 8, 10, 100, mHamRg);  			
			mHamPS.addParticleInitializer(new VelocityInitializer(G.getX(-140), G.getX(140), G.getY(-140), G.getY(140)));			
			mHamPS.addParticleModifier(new ScaleModifier(1f, 0f, 0f, 0.7f));	
			mHamPS.addParticleModifier(new ExpireModifier(0.7f));
			mScene.getLayer(2).addEntity(mHamPS);
			mEngine.getTextureManager().loadTexture(mHamTx);
	        
			Texture mOverTx = new Texture(P256, P256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mOverRg = TextureRegionFactory.createFromAsset(mOverTx, this, "gameover.png", 0, 0);
			mOverSp = new Sprite(G.getX(120), G.getY(83), G.getX(240), G.getY(154), mOverRg);			
			mEngine.getTextureManager().loadTexture(mOverTx);			
			
			Texture mTenTx = new Texture(P64, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mTenRg = TextureRegionFactory.createFromAsset(mTenTx, this, "minusten.png", 0, 0);
			mTenSp = new Sprite(-1000, 0, G.getX(40), G.getY(40), mTenRg);	
			mScene.getLayer(2).addEntity(mTenSp);
			mEngine.getTextureManager().loadTexture(mTenTx);	
			
	    //CAPA 3
	        Texture mInactiveTx = new Texture(P8, P8, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mInactiveRg = new TextureRegion(mInactiveTx, 0, 0, 0, 0);
			mInactiveSp = new Sprite(0, 0, G.getX(480), G.getY(320), mInactiveRg);
			mInactiveSp.setColor(0.2f, 0.2f, 0.2f, 0.9f);
			mScene.getLayer(3).addEntity(mInactiveSp);
			mInactiveSp.setVisible(false);
	        
			Texture mPauseTx = new Texture(P64, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mPauseRg = TextureRegionFactory.createFromAsset(mPauseTx, this, "btnpause.png", 0, 0);
			mPauseSp = new Sprite(0, G.getY(270), G.getX(50), G.getY(50), mPauseRg);	
			mScene.getLayer(3).addEntity(mPauseSp);
			mEngine.getTextureManager().loadTextures(mPauseTx);				
			
			Texture mMenuTx = new Texture(P64, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mMenuRg = TextureRegionFactory.createFromAsset(mMenuTx, this, "btnmenu.png", 0, 0);
			mMenuSp = new Sprite(G.getX(155), G.getY(135), G.getX(50), G.getY(50), mMenuRg);	
			mScene.getLayer(3).addEntity(mMenuSp);
			mEngine.getTextureManager().loadTextures(mMenuTx);
			mMenuSp.setPosition(G.getX(480), G.getY(135));
			
			Texture mPlayTx = new Texture(P64, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mPlayRg = TextureRegionFactory.createFromAsset(mPlayTx, this, "btnplay.png", 0, 0);
			mPlaySp = new Sprite(G.getX(215), G.getY(135), G.getX(50), G.getY(50), mPlayRg);	
			mScene.getLayer(3).addEntity(mPlaySp);
			mEngine.getTextureManager().loadTextures(mPlayTx);
			mPlaySp.setPosition(G.getX(480), G.getY(135));
			
			Texture mRestartTx = new Texture(P64, P64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TextureRegion mRestartRg = TextureRegionFactory.createFromAsset(mRestartTx, this, "btnrestart.png", 0, 0);
			mRestartSp = new Sprite(G.getX(275), G.getY(135), G.getX(50), G.getY(50), mRestartRg);	
			mScene.getLayer(3).addEntity(mRestartSp);
			mEngine.getTextureManager().loadTextures(mRestartTx);	
			mRestartSp.setPosition(G.getX(480), G.getY(135));	
			
			Texture mComboTx = new Texture(P256, P256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			TiledTextureRegion mComboRg = TextureRegionFactory.createTiledFromAsset(mComboTx, this, "combos.png", 0, 0, 1, 4);
			mComboSp = new TiledSprite(-1000, 0, G.getX(160), G.getY(48), mComboRg);
			mComboSp.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);		
			mComboSp.setCurrentTileIndex(0);
			mScene.getLayer(3).addEntity(mComboSp);
			mEngine.getTextureManager().loadTexture(mComboTx);						
			
		//SONIDOS
			try {
				SoundFactory.setAssetBasePath("sounds/");
				mMusic = MusicFactory.createMusicFromAsset(mEngine.getMusicManager(), this, "sounds/background.ogg");				
				mMusic.setLooping(true);				
				mMusic.setVolume(G.volume*0.4f);
				mMusic.play();	
				
				mBombon = MusicFactory.createMusicFromAsset(mEngine.getMusicManager(), this, "sounds/bombon.ogg");
				mBombon.setVolume(G.volume*0.4f);
				
				mSounds = new Sound[24];
				mSounds[0] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "yell.ogg");			
				mSounds[1] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "throwin.ogg");
				mSounds[2] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "impact1.ogg");
				mSounds[3] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "impact2.ogg");
				mSounds[4] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "impact3.ogg");
				mSounds[5] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "impact4.ogg");
				mSounds[6] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "impact5.ogg");
				mSounds[7] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "impact6.ogg");
				mSounds[8] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "splatter1.ogg");
				mSounds[9] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "splatter2.ogg");
				mSounds[10] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "combo4.ogg");
				mSounds[11] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "combo5.ogg");
				mSounds[12] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "combo6.ogg");
				mSounds[13] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "combo7.ogg");
				mSounds[14] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "tick.ogg");
				mSounds[15] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "tock.ogg");
				mSounds[16] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "timeup.ogg");
				mSounds[17] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "buttongame.ogg");
				mSounds[18] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "newbest.ogg");
				mSounds[19] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "sword1.ogg");
				mSounds[20] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "sword2.ogg");
				mSounds[21] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "sword3.ogg");
				mSounds[22] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "throwbomb.ogg");
				mSounds[23] = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "smallexplode.ogg");	
				for(byte i=0; i<mSounds.length; i++)
					mSounds[i].setVolume(G.volume);
			} 
			catch (Exception e) {}			
	}

	
	//---------------------------------------AL CARGAR LA ESCENA
	private final Vector2 v2=new Vector2(0,7), v22=new Vector2();
	private final Body bodies[]=new Body[50], bombs[]=new Body[4];
	private final byte scales[]=new byte[50], indexes[]=new byte[50], letCut[]=new byte[50];
	private final boolean lines[]=new boolean[20], blurs[]=new boolean[12];
	private final float toDeg=57.295779f;	
	private final ArrayList<Integer>order=new ArrayList<Integer>();	
	private final World mWorld=new World(v2, true);
	private final Timer timer=new Timer();	
	private byte nPts=0, ran, freeBody, freeBomb, toDecTime=30, min, seg1, seg2, time=60, freeBlur, numCuts=0, toClearCuts=0, toFadeCombo=0, waitBig=30, waitSmall=4, num=-1, waitSound=0, toResetFire=0;	
	private float scaleSword, alphaBlur, segs, angleBody;		
	
	public Scene onLoadScene() { 
		final CircleShape circle = new CircleShape();
	    final BodyDef bodyDef = new BodyDef();
	    final FixtureDef fixtureDef = new FixtureDef();		 
		 
	    for(byte i=0; i<50; i++){
	        circle.setRadius(1);               
	        bodyDef.position.x = -1000;
	        bodyDef.position.y = 1000;        
	        bodyDef.type = BodyType.DynamicBody;	        
	        bodies[i] = mWorld.createBody(bodyDef);	        
	        fixtureDef.shape = circle;
	        fixtureDef.density = 0.1f/(float)(Math.PI*1*1);	//100 gramos
	        fixtureDef.restitution = 1f;
	        bodies[i].createFixture(fixtureDef);	        
	        bodies[i].setActive(false);	
	        scales[i] = 40;
			indexes[i] = -1;
			letCut[i] = 0;
	    }
	    for(byte i=0; i<4; i++){
	        circle.setRadius(1);               
	        bodyDef.position.x = -1000;
	        bodyDef.position.y = 1000;        
	        bodyDef.type = BodyType.DynamicBody;	        
	        bombs[i] = mWorld.createBody(bodyDef);	        
	        fixtureDef.shape = circle;
	        fixtureDef.density = 0.1f/(float)(Math.PI*1*1);	//100 gramos
	        fixtureDef.restitution = 1f;
	        bombs[i].createFixture(fixtureDef);	        
	        bombs[i].setActive(false);	        
	    }
	    circle.dispose();
	    
	    //valores iniciales
	    mPizzaSp = new Sprite[50];
		
		for(byte i=0; i<20; i++){
			lines[i] = false;			
			if(i<12)
				blurs[i] = false;			
		}
		order.add(40);
		order.add(97);
		order.add(154);
		order.add(211);
		order.add(268);
		order.add(325);
		order.add(382);
		order.add(440);
		
		updateScores();		
		mSounds[0].play();
		
		final Handler handler = new Handler();
		Runnable r = new Runnable(){
		    public void run(){
		    	if(mBackgroundTx.isLoadedToHardware()){
					mEngine.setScene(mScene);
					mEngine.registerUpdateHandler(handleGame);	
					
					mScene.getLayer(3).registerTouchArea(mPauseSp);
					mScene.getLayer(3).registerTouchArea(mMenuSp);
					mScene.getLayer(3).registerTouchArea(mPlaySp);
					mScene.getLayer(3).registerTouchArea(mRestartSp);
					mScene.setOnAreaTouchListener(Arcade.this);				
					mScene.setOnSceneTouchListener(Arcade.this);
					handler.removeCallbacks(this);
				 }
				 else							
					handler.postDelayed(this, 1000);
		    }
		};
		handler.postDelayed(r, 1000);
	      
		return this.mScene;
	}	
	private final TimerHandler handleGame = new TimerHandler(1/30f, true, new ITimerCallback(){
		public void onTimePassed(final TimerHandler pTimerHandler) {				
	    	for(byte i=0; i<8; i++) 
	    		mWorld.step(1/30f, 8, 3);  
	    		   
	    	//tiempo
			if(--toDecTime==0){		
				toDecTime = 30;	
				
				if(--time<0){
					gameOver();					
					return;
				}				
				
				segs = time/60f;				
				min = (byte)segs;
				segs = Math.round((segs-min)*60f);
				seg1 = (byte) (segs/10f);
				seg2 = (byte) (segs-seg1*10f);
				mTimeSp[0].setCurrentTileIndex(min);
				mTimeSp[2].setCurrentTileIndex(seg1);
				mTimeSp[3].setCurrentTileIndex(seg2);
			}			
			else if(time<10){
				if(toDecTime==29){
					mSounds[14].play();				
					mTimeSp[0].setColor(1f, 0.2f, 0f);
					mTimeSp[1].setColor(1f, 0.2f, 0f);
					mTimeSp[2].setColor(1f, 0.2f, 0f);
					mTimeSp[3].setColor(1f, 0.2f, 0f);
				}
				else if(toDecTime==14){
					mSounds[15].play();
					mTimeSp[0].setColor(1f, 1f, 0f);
					mTimeSp[1].setColor(1f, 1f, 0f);
					mTimeSp[2].setColor(1f, 1f, 0f);
					mTimeSp[3].setColor(1f, 1f, 0f);
				}
			}
			
			//lanzamiento
	    	throwIn();
	    	
	    	//bombas
	    	for(byte i=0; i<4; i++){
	    		if(bombs[i].isActive()){
	    			switch(mBombSp[i].getCurrentTileIndex()){
	    				case 0:		mBombSp[i].setCurrentTileIndex(1);
	    								break;
	    				case 1:		mBombSp[i].setCurrentTileIndex(2);
										break;
	    				case 2:		mBombSp[i].setCurrentTileIndex(0);
	    			}	    
	    			
	    			if(bombs[i].getAngularVelocity()>1f)
	    				bombs[i].setAngularVelocity(1f);
	    			else if(bombs[i].getAngularVelocity()<-1f)
	    				bombs[i].setAngularVelocity(-1f);
	    			
	    			v22.set(bombs[i].getPosition());			//sin escalar
	    			v2.set(G.getX(v22.x), G.getY(v22.y));	//escalado    	
	    			
	    			mBombSp[i].setPosition(v2.x-G.getX(15), v2.y-G.getY(45));
	    			mBombSp[i].setRotation(bombs[i].getAngle()*toDeg);
	    			
	    			for(byte k=0; k<20; k++){	
	    				if(mBombSp[i].collidesWith(mLineSp[k])){	    						    					
	    					time-=10;	    					
	    					if(time<1)
	    						time = 1;	    						
	    						
	    					toDecTime = 1;
	    					bombs[i].setActive(false);        			
		        			mBombSp[i].setPosition(-1000,0);	
		        			mBombon.pause();	
		        			mSounds[23].play();
		        			
		        			mFirePE.setCenter(v2.x, v2.y);
		        			mTenSp.setPosition(v2.x-G.getX(10), v2.y-G.getY(10));
		        			toResetFire = 30;
		        			break;
	    				}
	    			}
	    			
					//elimina bombas
		    		if(v2.y>G.getY(400)){
	        			bombs[i].setActive(false);        			
	        			mBombSp[i].setPosition(0,-1000);	        				 
	        		}
	    		}
	    	}
	    	
	    	if(toResetFire>0){
	    		if(--toResetFire==0){
	    			mFirePE.setCenter(-1000, 0);
	    			mTenSp.setPosition(-1000, 0);
	    		}
	    	}

	    	for(byte i=0; i<50; i++){
	    		if(bodies[i].isActive()){	    	
	    			if(bodies[i].getAngularVelocity()>1f)
	    				bodies[i].setAngularVelocity(1f);
	    			else if(bodies[i].getAngularVelocity()<-1f)
	    				bodies[i].setAngularVelocity(-1f);
	    			
	    			if(--scales[i]<-40)
	    				scales[i] = -40;	    			
	    			else if(scales[i]==4){
	    				mScene.getLayer(1).removeEntity(mPizzaSp[i]);
	    					    				
	    				if(indexes[i]<12)	    					
	    					mPizzaSp[i] = new Sprite(0, -1000, G.getX(8), G.getY(84), mPxxxRg.clone());
	    				else
	    					mPizzaSp[i] = new Sprite(0, -1000, G.getX(8), G.getY(42), mPxxxxxxRg.clone());
	    					
	        			mScene.getLayer(1).addEntity(mPizzaSp[i]);
	    			}
	    			else if(scales[i]==-5){
	    				mScene.getLayer(1).removeEntity(mPizzaSp[i]);	    				
	    				
	    				if(indexes[i]<6)	    				
	    					mPizzaSp[i] = new Sprite(0, -1000, G.getX(84), G.getY(84), mPxRg.clone());
	    				else if(indexes[i]<12)
	    					mPizzaSp[i] = new Sprite(0, -1000, G.getX(42), G.getY(84), mPxxRg.clone());
	    				else if(indexes[i]<18)
	    					mPizzaSp[i] = new Sprite(0, -1000, G.getX(42), G.getY(42), mPxxxxRg.clone());
	    				else
	    					mPizzaSp[i] = new Sprite(0, -1000, G.getX(32), G.getY(42), mPxxxxxRg.clone());
	    				
	        			mScene.getLayer(1).addEntity(mPizzaSp[i]);	        			
	    			}

	    			v22.set(bodies[i].getPosition());			//sin escalar
	    			v2.set(G.getX(v22.x), G.getY(v22.y));	//escalado    				    			
	    			mPizzaSp[i].setRotation(bodies[i].getAngle()*toDeg);
	    			
		    		if(Math.abs(scales[i])<5){		        		
		        		if(indexes[i]<12)
		        			mPizzaSp[i].setPosition(v2.x-G.getX(4), v2.y-G.getY(42));
		        		else
		        			mPizzaSp[i].setPosition(v2.x-G.getX(4), v2.y-G.getY(21));
	        		}	
		    		else if(scales[i]>0){		        				        		
		        		if(indexes[i]<6)		        		
		        			mPizzaSp[i].setPosition(v2.x-G.getX(42), v2.y-G.getY(42));
		        		else if(indexes[i]<12)
		        			mPizzaSp[i].setPosition(v2.x-G.getX(21), v2.y-G.getY(42));
		        		else if(indexes[i]<18)
		        			mPizzaSp[i].setPosition(v2.x-G.getX(21), v2.y-G.getY(21));
		        		else 
		        			mPizzaSp[i].setPosition(v2.x-G.getX(16), v2.y-G.getY(21));
		        		
		        		mPizzaSp[i].setScaleX(scales[i]/40f);			        		
	        		}
	        		else{	        			        				        			
	        			if(indexes[i]<6)
	        				mPizzaSp[i].setPosition(v2.x-G.getX(42), v2.y-G.getY(42));
	        			else if(indexes[i]<12)
		        			mPizzaSp[i].setPosition(v2.x-G.getX(21), v2.y-G.getY(42));
	        			else if(indexes[i]<18)
		        			mPizzaSp[i].setPosition(v2.x-G.getX(21), v2.y-G.getY(21));
	        			else 
		        			mPizzaSp[i].setPosition(v2.x-G.getX(16), v2.y-G.getY(21));
	        			
	        			mPizzaSp[i].setScaleX(-scales[i]/40f);		        		
	        		}
		    		
		    		//colisiones
			    	if(indexes[i]<18 && nPts>0 && letCut[i]==0){
			    		for(byte k=0; k<20; k++){			    			
			    			if(mPizzaSp[i].collidesWith(mLineSp[k])){			    				
			    				mPizzaSp[i].reset();
			        			mScene.getLayer(1).removeEntity(mPizzaSp[i]);
			    							    						    				    						
	    						ran = (byte) (indexes[i]+6);
			    				freeBody = searchBody();		
			    				bodies[freeBody].setActive(true);
			    				
			    				mTomatoPE.setCenter(v2.x, v2.y);
			    				mChampignonPE.setCenter(v2.x, v2.y);
			    				mHamPE.setCenter(v2.x, v2.y);
			    				
			    				angleBody = mLineSp[k].getRotation()/toDeg;
			    				bodies[i].setTransform(v22, angleBody+1.57f);
			    				bodies[freeBody].setTransform(v22, angleBody+4.71f);			    				
			    				bodies[freeBody].setLinearVelocity(-bodies[i].getLinearVelocity().x, bodies[i].getLinearVelocity().y);
			    				bodies[freeBody].setAngularVelocity((float)Math.random());
			    				
			    				bodies[freeBody].setActive(true);
			    				letCut[i] = 20;
			    				letCut[freeBody] = 20;
			    						
			    				if(ran<6){
			    					mPizzaSp[i] = new Sprite(0, -1000, G.getX(84), G.getY(84), mPRg[ran].clone());
			    					mPizzaSp[freeBody] = new Sprite(0, -1000, G.getX(84), G.getY(84), mPRg[ran].clone());
			    				}
			    				else if(ran<12){
			    					mPizzaSp[i] = new Sprite(0, -1000, G.getX(42), G.getY(84), mPRg[ran].clone());
			    					mPizzaSp[freeBody] = new Sprite(0, -1000, G.getX(42), G.getY(84), mPRg[ran].clone());
			    				}
			    				else if(ran<18){
			    					mPizzaSp[i] = new Sprite(0, -1000, G.getX(42), G.getY(42), mPRg[ran].clone());
			    					mPizzaSp[freeBody] = new Sprite(0, -1000, G.getX(42), G.getY(42), mPRg[ran].clone());
			    				}
			    				else{
			    					mPizzaSp[i] = new Sprite(0, -1000, G.getX(32), G.getY(42), mPRg[ran].clone());
			    					mPizzaSp[freeBody] = new Sprite(0, -1000, G.getX(32), G.getY(42), mPRg[ran].clone());
			    				}
			    				
			    				indexes[i] = ran;
			    				indexes[freeBody] = ran;
			    				
			    				scales[freeBody] = scales[i];
			    				
			    				mScene.getLayer(1).addEntity(mPizzaSp[i]);
			    				mScene.getLayer(1).addEntity(mPizzaSp[freeBody]);	
			    				
			    				//manchas
			    				freeBlur = searchBlur();
				    			if(freeBlur>-1){
				    				blurs[freeBlur] = true;				    				
				    				mScene.getLayer(0).addEntity(mBlurSp[freeBlur]);
				    				mBlurSp[freeBlur].setPosition(mLineSp[k]);
				    				mBlurSp[freeBlur].setWidth(mLineSp[k].getWidth()*3);
				    				
				    				if(mBlurSp[freeBlur].getWidth()<G.getX(96))
				    					mBlurSp[freeBlur].setWidth(G.getX(96));
				    				
				    				mBlurSp[freeBlur].setRotation(mLineSp[k].getRotation());
			    				}
				    							    	
				    			if(++numCuts==1)
				    				toClearCuts = 20;
				    							    			
				    			mSounds[getRandom(2,7)].play();				    			
			    				break;
			    			}			    			
			    		}
			    	}
		    	
			    	//elimina pizzas
		    		if(v2.y>G.getY(400)){
	        			bodies[i].setActive(false);        			
	        			mPizzaSp[i].reset();
	        			mScene.getLayer(1).removeEntity(mPizzaSp[i]);	 
	        		}
	    		}
	    		
	    		if(letCut[i]>0){	    			
	    			if(--letCut[i]==10){
	    				mTomatoPE.setCenter(-1000, 0);
	    				mChampignonPE.setCenter(-1000, 0);
	    				mHamPE.setCenter(-1000, 0);
	    			}
	    		}
	    	}	   
	    	
	    	//combos
	    	if(toClearCuts>0){	   	    		
	    		if(--toClearCuts==0){
		    		if(numCuts<4)
		    			score+=numCuts;
		    		else{
		    			if(numCuts==4){
		    				mSounds[8].play();
		    				mSounds[10].play();
		    			}
		    			else if(numCuts==5){
		    				mSounds[8].play();
		    				mSounds[11].play();
		    			}
		    			else if(numCuts==6){
		    				mSounds[9].play();
		    				mSounds[12].play();
		    			}
		    			else if(numCuts>=7){
		    				numCuts = 7;
		    				mSounds[9].play();
		    				mSounds[13].play();
		    			}
		    			
		    			if(freeBlur>-1){
		    				mComboSp.setPosition(mBlurSp[freeBlur]);
		    				if(mComboSp.getX()>G.getX(290) || mComboSp.getY()>G.getY(272))
		    					mComboSp.setPosition(G.getX(147), G.getY(100));
		    			}
		    			else
		    				mComboSp.setPosition(G.getX(147), G.getY(100));
		    			
			    		mComboSp.setCurrentTileIndex(numCuts-4);
			    		toFadeCombo = 40;
			    		score+=(numCuts*numCuts);			    		
		    		}		    		
		    		
		    		numCuts = 0;
		    		updateScores();
		    	}
	    	}
	    	if(toFadeCombo>0)
	    		if(--toFadeCombo==0)
	    			mComboSp.setPosition(-1000, 0);
	    	
	    	//actualiza manchas
	    	for(byte i=0; i<12; i++){
	    		if(blurs[i]){
	    			alphaBlur = mBlurSp[i].getAlpha()-0.008f;
	    			
	    			if(alphaBlur>0f)
	    				mBlurSp[i].setAlpha(alphaBlur);
	    			else{	    					    				
	    				mScene.getLayer(0).removeEntity(mBlurSp[i]);
	    				mBlurSp[i].setAlpha(1f);
	    				blurs[i] = false;
	    			}
	    		}	    			
	    	}
	    	
	    	//espada	    	
    		for(byte i=0; i<20; i++){
    			if(lines[i]){    			    				
	    			scaleSword = mPointSp[i].getScaleX()-0.1f;	    			
	    			
	    			if(scaleSword>0.1f){
	    				mPointSp[i].setScale(scaleSword);
	    				mLineSp[i].setScaleY(scaleSword);
	    			}
	    			else{
	    				mPointSp[i].setPosition(-1000, 0);
	    				mLineSp[i].setPosition(-1000, 0);
	    				mScene.getLayer(2).removeEntity(mPointSp[i]);
	    				mScene.getLayer(2).removeEntity(mLineSp[i]);
	    				mPointSp[i].setScale(1f);
	    				mLineSp[i].setScaleY(1f);
	    				lines[i] = false;
	    				mSparkPE.setCenter(-1000, 0);	    				
	    				nPts--;
	    			}	    		
    			}
    		}	
    		
    		//sonido espada
    		if(waitSound>0)
    			waitSound--;
	    }
	});	
	public void throwIn(){
		if(time<2)
			return;
		
		if(num==-1){
			if(--waitBig==0){		
				num = (byte) getRandom(2,7);				
				Collections.shuffle(order);
				waitBig = (byte) getRandom(60,90);
			}
		}
		else{
			if(--waitSmall==0){	
				if(Math.random()<0.8f){
					ran = (byte) getRandom(0,17);						
					freeBody = searchBody();			
							
					bodies[freeBody].setActive(true);
					bodies[freeBody].setTransform(order.get(num), 320, (float)Math.random());
					
					if(order.get(num)<240)
						bodies[freeBody].applyLinearImpulse(getRandom(0,1500), getRandom(-8000,-2000), 0, 0);				
					else
						bodies[freeBody].applyLinearImpulse(getRandom(-1500,0), getRandom(-8000,-2000), 0, 0);				
					
					bodies[freeBody].setAngularVelocity((float) (2*Math.random()-1f));
							
					if(ran<6)
						mPizzaSp[freeBody] = new Sprite(0, -1000, G.getX(84), G.getY(84), mPRg[ran].clone());
					else if(ran<12)
						mPizzaSp[freeBody] = new Sprite(0, -1000, G.getX(42), G.getY(84), mPRg[ran].clone());
					else 
						mPizzaSp[freeBody] = new Sprite(0, -1000, G.getX(42), G.getY(42), mPRg[ran].clone());
					
					indexes[freeBody] = ran;
					scales[freeBody] = 40;
					mScene.getLayer(1).addEntity(mPizzaSp[freeBody]);
					
					waitSmall = (byte) getRandom(3,30);
					num--;
					
					mSounds[1].play();
				}
				else{										
					freeBomb = searchBomb();			
					
					if(freeBomb==-1){
						waitSmall = (byte) getRandom(3,30);
						return;				
					}
						
					mBombSp[freeBomb].setScale((float) (Math.random()+0.5f));
					bombs[freeBomb].setActive(true);
					bombs[freeBomb].setTransform(order.get(num), 320, (float)Math.random());
					
					if(order.get(num)<240)
						bombs[freeBomb].applyLinearImpulse(getRandom(0,1500), getRandom(-8000,-2000), 0, 0);				
					else
						bombs[freeBomb].applyLinearImpulse(getRandom(-1500,0), getRandom(-8000,-2000), 0, 0);				
					
					bombs[freeBomb].setAngularVelocity((float) (2*Math.random()-1f));					
					waitSmall = (byte) getRandom(3,30);
					num--;
					
					mSounds[22].play();										
					mBombon.play();	
				}
			}
		}		
	}
	public byte searchBody(){
		for(byte i=0; i<50; i++){
			if(!bodies[i].isActive())				
				return i;					
		}		
		return 0;
	}
	public byte searchBomb(){
		for(byte i=0; i<4; i++){
			if(!bombs[i].isActive())				
				return i;					
		}		
		return -1;
	}
	public byte searchBlur(){
		byte idx = (byte) (Math.random()*11);
		if(!blurs[idx])
			return idx;
		for(byte i=0; i<12; i++){
			if(!blurs[i])				
				return i;					
		}		
		return -1;
	}
	public void updateScores(){				
		if(score>best)
			best = score;	
		
		byte array[] = String.valueOf(score).getBytes();
		for(byte i=0; i<array.length; i++){
			mScoreSp[i].setCurrentTileIndex(array[i]-48);
			mScoreSp[i].setVisible(true);
		}
					
		array = String.valueOf(best).getBytes();
		for(byte i=0; i<array.length; i++){
			mBestSp[i].setCurrentTileIndex(array[i]-48);
			mBestSp[i].setVisible(true);				
		}		
	}
	public void gameOver(){
		runOnUiThread(new Runnable() {
			public void run(){
				mEngine.unregisterUpdateHandler(handleGame);													
			}
		});	
		
		mPauseSp.setPosition(0, G.getY(320));
		pause = true;
		
		mScene.getLayer(1).clear();
		mScene.getLayer(2).clear();
		mComboSp.setPosition(-1000,0);
		mOverSp.addShapeModifier(new org.anddev.andengine.entity.shape.modifier.ScaleModifier(1f, 2f, 1f, 2f, 1f));
		mScene.getLayer(2).addEntity(mOverSp);
		mSounds[16].play();		
		
		SharedPreferences.Editor editor;
		editor = settings.edit();
		editor.putInt("bestArcade", best);		
		editor.commit();
				
		TimerTask timerFunc = new TimerTask()
		{
			
			public void run() {				
				mInactiveSp.setVisible(true);
				mMenuSp.reset();																		
				mRestartSp.reset();	
				
				if(score>=best){					
					Leaderboard l = new Leaderboard("954476");
					Score s = new Score((long)score, null);
					s.submitTo(l, null);
					mSounds[18].play();
				}
			}
		};   				
		timer.schedule(timerFunc, 3000);
	}
	
		
	//---------------------------------------AL HACER TOUCH EN LA PANTALLA
	private float xBef, yBef, x, y, dst;
	private boolean pause=false, waitSword=false;
	private byte freeLine=0;
	
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent event) {	
		if(pause)
			return true;
		
		if(event.getAction()==TouchEvent.ACTION_UP)
			waitSword = true;
		
		if(waitSword){
			if(nPts>0)
				return true;
			else
				waitSword = false;				
		}
		
		freeLine = searchLine();		
		if(freeLine==-1)
			return true;
		
		x = event.getX();
		y = event.getY();
		
		if(nPts==1){
			xBef = x;
			yBef =  y;
		}
				
		mSparkPE.setCenter(x, y);		
		mPointSp[freeLine].setPosition(x-G.getX(5), y-G.getY(5));			
		mScene.getLayer(2).addEntity(mPointSp[freeLine]);
		
		dst = (float) Math.hypot(x-xBef, y-yBef);			
		if(dst<1f)
			dst = 1f;			
		mLineSp[freeLine].setPosition(xBef, yBef-G.getY(5));	
		mLineSp[freeLine].setWidth(dst);
		mLineSp[freeLine].setRotation((float)(toDeg*Math.atan2(y-yBef, x-xBef)));		    							
		mScene.getLayer(2).addEntity(mLineSp[freeLine]);
		
		xBef = x;
		yBef = y;					
		return true;
	}
	public byte searchLine(){
		for(byte i=0; i<20; i++){
			if(!lines[i]){
				lines[i] = true;
				nPts++;
				if(waitSound==0){
					mSounds[getRandom(19,21)].play();
					waitSound = 10;					
				}
				return i;
			}
		}		
		return -1;
	}

	
	//---------------------------------------AL HACER TOUCH EN UN BOT�N	
	
	public boolean onAreaTouched(TouchEvent pEvent,ITouchArea pArea, float pTouchAreaLocalX,float pTouchAreaLocalY) {	
		if(pEvent.getAction()==TouchEvent.ACTION_DOWN){			
			if(pArea.equals(mPauseSp) && !pause){							
				mPauseSp.addShapeModifier(new ColorModifier(0.01f, 0f, 1.5f, 0f, 1.5f, 0f, 1.5f));
				mEngine.unregisterUpdateHandler(handleGame);				
				pause = true;
				iniDelay(0);								
			}
			else if(pArea.equals(mMenuSp) && pause){								
				mMenuSp.addShapeModifier(new ColorModifier(0.01f, 0f, 1.5f, 0f, 1.5f, 0f, 1.5f));
				pause = false;
				iniDelay(1);				
			}
			else if(pArea.equals(mPlaySp) && pause){								
				mPlaySp.addShapeModifier(new ColorModifier(0.01f, 0f, 1.5f, 0f, 1.5f, 0f, 1.5f));
				pause = false;
				iniDelay(2);
			}
			else if(pArea.equals(mRestartSp) && pause){								
				mRestartSp.addShapeModifier(new ColorModifier(0.01f, 0f, 1.5f, 0f, 1.5f, 0f, 1.5f));
				pause = false;
				iniDelay(3);
			}
		}
		return true;
	}
	public void iniDelay(final int mode){	
		mSounds[17].play();
		
		TimerTask timerFunc = new TimerTask()
		{
			
			public void run() {				
				switch(mode){				
					case 0:		mInactiveSp.setVisible(true);
									mPauseSp.setPosition(0, G.getY(320));																			
									mMenuSp.reset();																		
									mPlaySp.reset();
									mRestartSp.reset();
									mComboSp.setPosition(-1000,0);
									break;
					case 1:		startActivity(new Intent(Arcade.this, Menu.class));
									finish();	
									break;						
					case 2:		mInactiveSp.setVisible(false);
									mPauseSp.reset();
									mMenuSp.setPosition(G.getX(480), G.getY(135));
									mPlaySp.setPosition(G.getX(480), G.getY(135));
									mRestartSp.setPosition(G.getX(480), G.getY(135));
									mEngine.registerUpdateHandler(handleGame);												
									break;		
					case 3:		startActivity(new Intent(Arcade.this, Arcade.class));
									finish();									
				}															
			}
		};   
				
		timer.schedule(timerFunc, 400);				
	}
	
	
	//---------------------------------------M�TODO OBTIENE UN # ALEATORIO ENTRE LINF Y LSUP, INCLU�DOS
	public int getRandom(double inf, double sup){
		return (int) Math.round((sup-inf)*Math.random()+inf);
	}	
	
	
	//---------------------------------------AL TERMINAR DE CARGAR		
	
	public void onLoadComplete() {}	
	
	
	//---------------------------AL PAUSAR	 
	 
	 public void onPause() {
		 super.onPause();
		 if(mMusic!=null)
			 mMusic.pause();
	 }
	 
	 
	 //---------------------------AL REANUDAR	 
	 
	 public void onResume() {
		 super.onResume();
		 if(mMusic!=null)
			 mMusic.resume();		
	 }	 
	 
	 
	 //---------------------------AL CERRAR	 
	 
	 public void onDestroy() {		 		 		 		 		 
		 super.onDestroy();	
	     mEngine.getScene().clearChildScene();	        
	     mEngine.getScene().clearTouchAreas();
	     mEngine.getScene().clearUpdateHandlers();	     
		 timer.cancel();	
		 
		 for(byte i=0; i<50; i++){		       	        
			 if(bodies[i].isActive()){				 
				 bodies[i].setActive(false);
				 mPizzaSp[i].reset();
				 mScene.getLayer(1).removeEntity(mPizzaSp[i]);
				 BufferObjectManager.getActiveInstance().unloadBufferObject(mPizzaSp[i].getVertexBuffer());
			 }			 
		 }
		 
		 for(byte i=0; i<4; i++){		              
			if(bombs[i].isActive()){				 
				 bombs[i].setActive(false);
				 mBombSp[i].reset();
				 mScene.getLayer(1).removeEntity(mBombSp[i]);
				 BufferObjectManager.getActiveInstance().unloadBufferObject(mBombSp[i].getVertexBuffer());
			}
		 }
		 
		 mWorld.dispose();
		 System.gc();		 		 
	 }	 
	 
	 
	 //---------------------------AL PRESIONAR BACK	 
	 
	 public void onBackPressed() {
		 System.exit(0);
	 }	 
}

