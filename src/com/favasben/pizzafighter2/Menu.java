package com.favasben.pizzafighter2;

import java.util.Timer;
import java.util.TimerTask;

import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.Scene.IOnAreaTouchListener;
import org.anddev.andengine.entity.scene.Scene.ITouchArea;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.sprite.TiledSprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.Toast;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.ui.Dashboard;



public class Menu extends BaseGameActivity implements IOnAreaTouchListener{
	
	
 	//------------------------------M�TODO AL CREAR LA ACTIVIDAD		
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);    	
    	overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);  
    	System.gc();   
	}	
 	
 	
 	//-----------------------------AL CARGAR EL MOTOR
    private Camera mCamera; 
 	
	 public Engine onLoadEngine() {
 		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mCamera = new Camera(0, 0, G.getX(480), G.getY(320));
		 
		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(G.getX(480), G.getY(320)), mCamera).setNeedsMusic(true).setNeedsSound(true);		 
		Engine mEngine = new Engine(engineOptions);		 

		return mEngine;
	 } 	
 	

	//---------------------------------------AL CARGAR LOS RECURSOS
 	private int P256, P512;
 	private Scene mScene;
 	
 	//capa 0
 	private Texture mBackgroundTx;
 	private Sprite mBackgroundSp;
 	private TiledSprite mPlaySp, mOptionsSp, mFeintSp; 	
 	
 	private Music mMusic=null;
 	private Sound mSound;
	
	public void onLoadResources() {			
		mScene = new Scene(1);
		
		if(G.hdpi){
			TextureRegionFactory.setAssetBasePath("hdpi/");
			P256 = 512;
			P512 = 1024;
		}
		else{
			TextureRegionFactory.setAssetBasePath("mdpi/");
			P256 = 256;
			P512 = 512;
		}	
		
		mBackgroundTx = new Texture(P512, P512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 		
		TextureRegion mBackgroundRg = TextureRegionFactory.createFromAsset(mBackgroundTx, this, "portada.png",0,0);
		mBackgroundSp = new Sprite(0, 0, G.getX(480), G.getY(320), mBackgroundRg);
		mScene.getLayer(0).addEntity(mBackgroundSp);
		mEngine.getTextureManager().loadTextures(mBackgroundTx);
				
		Texture mPlayTx = new Texture(P256, P256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
		TiledTextureRegion mPlayRg = TextureRegionFactory.createTiledFromAsset(mPlayTx, getApplicationContext(), "play.png", 0, 0, 1, 3);
		mPlaySp = new TiledSprite(-1000, -1000, G.getX(200), G.getY(50), mPlayRg);
		mPlaySp.setRotationCenter(G.getX(100), G.getY(25));
		mScene.getLayer(0).addEntity(mPlaySp);
		mEngine.getTextureManager().loadTextures(mPlayTx);
		
		Texture mOptionsTx = new Texture(P256, P256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
		TiledTextureRegion mOptionsRg = TextureRegionFactory.createTiledFromAsset(mOptionsTx, getApplicationContext(), "options.png", 0, 0, 1, 3);
		mOptionsSp = new TiledSprite(-1000, -1000, G.getX(200), G.getY(50), mOptionsRg);
		mOptionsSp.setRotationCenter(G.getX(100), G.getY(25));
		mScene.getLayer(0).addEntity(mOptionsSp);
		mEngine.getTextureManager().loadTextures(mOptionsTx);
		
		Texture mFeintTx = new Texture(P256, P256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);		 
		TiledTextureRegion mFeintRg = TextureRegionFactory.createTiledFromAsset(mFeintTx, getApplicationContext(), "feint.png", 0, 0, 1, 3);
		mFeintSp = new TiledSprite(-1000, -1000, G.getX(200), G.getY(50), mFeintRg);
		mFeintSp.setRotationCenter(G.getX(100), G.getY(25));
		mScene.getLayer(0).addEntity(mFeintSp);
		mEngine.getTextureManager().loadTextures(mFeintTx);	
		
	//SONIDOS
		try {			
			mMusic = MusicFactory.createMusicFromAsset(mEngine.getMusicManager(), this, "sounds/menu.ogg");				
			mMusic.setLooping(true);				
			mMusic.setVolume(G.volume*0.6f);
			mMusic.play();	
			
			mSound = SoundFactory.createSoundFromAsset(mEngine.getSoundManager(), this, "sounds/buttonmenu.ogg");						
			mSound.setVolume(G.volume);
		} 
		catch (Exception e) {}
	}
	
	
	//---------------------------------------AL CARGAR LA ESCENA
	private final Vector2 v2=new Vector2(0,10);
	private final World mWorld=new World(v2, true);
	private final Body bodies[]=new Body[3];
	private final float toDeg = 57.295779f;  	
	private byte buttonId=0, toCButton=3; 	
	
	public Scene onLoadScene() {
		final PolygonShape poly = new PolygonShape();
	    final BodyDef bodyDef = new BodyDef();
	    final FixtureDef fixtureDef = new FixtureDef();
		
		for(byte i=0; i<3; i++){
	        poly.setAsBox(100, 25); 	        
	        bodyDef.type = BodyType.DynamicBody;	        
	        bodies[i] = mWorld.createBody(bodyDef);	        
	        fixtureDef.shape = poly;
	        fixtureDef.density = 0.1f/(200*50);	//100 gramos
	        fixtureDef.restitution = 0.5f;
	        bodies[i].createFixture(fixtureDef);	        
	        bodies[i].setActive(false);	
		}        
        poly.dispose();
		
        final Handler handler = new Handler();
		Runnable r = new Runnable(){
		    public void run(){
		    	if(mBackgroundTx.isLoadedToHardware()){
					mEngine.setScene(mScene);
					mEngine.registerUpdateHandler(handleMenu);	
					
					resetScene();							
					handler.removeCallbacks(this);
				 }
				 else							
					handler.postDelayed(this, 1000);
		    }
		};
		handler.postDelayed(r, 1000);
                	
		return mScene;
	}
	private TimerHandler handleMenu = new TimerHandler(1/30f, true, new ITimerCallback(){
		public void onTimePassed(final TimerHandler pTimerHandler) {						
	    	for(byte i=0; i<8; i++) 
	    		mWorld.step(1/30f, 8, 3);  
	    
	    	if(bodies[0].isActive()){
	    		v2.set(bodies[0].getPosition());
	    		mPlaySp.setPosition(G.getX(v2.x-100), G.getY(v2.y-25));
	    		mPlaySp.setRotation(bodies[0].getAngle()*toDeg);
	    	}
	    	
	    	if(bodies[1].isActive()){
	    		v2.set(bodies[1].getPosition());
	    		mOptionsSp.setPosition(G.getX(v2.x-100), G.getY(v2.y-25));
	    		mOptionsSp.setRotation(bodies[1].getAngle()*toDeg);
	    	}
	    	
	    	if(bodies[2].isActive()){
	    		v2.set(bodies[2].getPosition());
	    		mFeintSp.setPosition(G.getX(v2.x-100), G.getY(v2.y-25));
	    		mFeintSp.setRotation(bodies[2].getAngle()*toDeg);
	    	}
	    	
	    	if(--toCButton==0){	    			    		
				toCButton = 3;
				if(++buttonId==3)
					buttonId = 0;
				
				mPlaySp.setCurrentTileIndex(buttonId);
				mOptionsSp.setCurrentTileIndex(buttonId);
				mFeintSp.setCurrentTileIndex(buttonId);				
			}	    		    	
		}
	});
	

	//---------------------------------------AL HACER TOUCH EN UN BOT�N	
	
	public boolean onAreaTouched(TouchEvent pEvent,ITouchArea pArea, float pTouchAreaLocalX,float pTouchAreaLocalY) {
		if(pEvent.getAction()==TouchEvent.ACTION_DOWN){			
			if(pArea.equals(mPlaySp)){
				bodies[0].setType(BodyType.StaticBody);
				bodies[0].setActive(true);				
				bodies[1].setActive(true);				
				bodies[1].setLinearVelocity(getRandom(-10,0),-10);
				bodies[2].setActive(true);
				bodies[2].setLinearVelocity(getRandom(0,10),-10);
				
				mSound.play();
				mPlaySp.addShapeModifier(new org.anddev.andengine.entity.shape.modifier.ColorModifier(0.1f, 0f, 1.5f, 0f, 1.5f, 0f, 1.5f));
				iniDelay(0);
			}
			else if(pArea.equals(mOptionsSp)){
				bodies[1].setType(BodyType.StaticBody);
				bodies[1].setActive(true);				
				bodies[0].setActive(true);
				bodies[0].setLinearVelocity(getRandom(0,10),-10);
				bodies[2].setActive(true);
				bodies[2].setLinearVelocity(getRandom(0,10),-10);	
				
				mSound.play();
				mOptionsSp.addShapeModifier(new org.anddev.andengine.entity.shape.modifier.ColorModifier(0.1f, 0f, 1.5f, 0f, 1.5f, 0f, 1.5f));
				iniDelay(1);
			}
			else if(pArea.equals(mFeintSp)){
				bodies[2].setType(BodyType.StaticBody);
				bodies[2].setActive(true);				
				bodies[1].setActive(true);
				bodies[1].setLinearVelocity(getRandom(-10,0),-10);
				bodies[0].setActive(true);
				bodies[0].setLinearVelocity(getRandom(-10,0),-10);	
				
				mSound.play();
				mFeintSp.addShapeModifier(new org.anddev.andengine.entity.shape.modifier.ColorModifier(0.1f, 0f, 1.5f, 0f, 1.5f, 0f, 1.5f));
				iniDelay(2);
			}			
		}
		
		return true;
	}
	
	public void iniDelay(final int mode){
		mScene.getLayer(0).unregisterTouchArea(mPlaySp);
		mScene.getLayer(0).unregisterTouchArea(mOptionsSp);
		mScene.getLayer(0).unregisterTouchArea(mFeintSp);
		
		final Timer timer = new Timer();
		TimerTask timerFunc = new TimerTask()
		{
			
			public void run() {
				timer.cancel();
								
				switch(mode){
					case 0:		startActivity(new Intent(Menu.this, Mode.class));
									finish();
									break;
					case 1:		startActivity(new Intent(Menu.this, Options.class));
									finish();
									break;									
					case 2:		if(OpenFeint.isNetworkConnected()){										
										if (!OpenFeint.isUserLoggedIn()) 
										    OpenFeint.login();									
										Dashboard.open();
										resetScene();
									}
									else{
										resetScene();
										Menu.this.runOnUiThread(new Runnable() {
											public void run(){
												Toast.makeText(Menu.this, "You are offline. Feint requires an active network connection", Toast.LENGTH_LONG).show();
											}
										});										
									}
				}															
			}
		};   				
		timer.schedule(timerFunc, 1500);				
	}
	public void resetScene(){
		bodies[0].setActive(false);
		bodies[1].setActive(false);
		bodies[2].setActive(false);		
		
		mPlaySp.reset();
		mOptionsSp.reset();
		mFeintSp.reset();
		
		mScene.getLayer(0).registerTouchArea(mPlaySp);
		mScene.getLayer(0).registerTouchArea(mOptionsSp);
		mScene.getLayer(0).registerTouchArea(mFeintSp);		
		mScene.setOnAreaTouchListener(this);			
		
		v2.set(240, 35);		   
		bodies[0].setTransform(v2, 0);        
		mPlaySp.setPosition(G.getX(v2.x-100), G.getY(v2.y-25));

		v2.set(100, 120);
		bodies[1].setTransform(v2, 0);        
		mOptionsSp.setPosition(G.getX(v2.x-100), G.getY(v2.y-25));
	    
		v2.set(380, 120);
		bodies[2].setTransform(v2, 0);        
		mFeintSp.setPosition(G.getX(v2.x-100), G.getY(v2.y-25));
		
		bodies[0].setType(BodyType.DynamicBody);
		bodies[1].setType(BodyType.DynamicBody);
		bodies[2].setType(BodyType.DynamicBody);
	}

	
	//---------------------------------------M�TODO OBTIENE UN # ALEATORIO ENTRE LINF Y LSUP, INCLU�DOS
	public int getRandom(double inf, double sup){
		return (int) Math.round((sup-inf)*Math.random()+inf);
	}	
	
	
	//---------------------------------------AL TERMINAR DE CARGAR 	
	
	public void onLoadComplete() {}	
	
	
	//---------------------------AL PAUSAR	 
	 
	 public void onPause() {
		 super.onPause();
		 if(mMusic!=null)
			 mMusic.pause();
	 }
	 
	 
	 //---------------------------AL REANUDAR	
	 
	 public void onResume() {
		 super.onResume();
		 if(mMusic!=null)
			 mMusic.resume();	
	 }	 
	 
	 
	 //---------------------------AL CERRAR	 
	 
	 public void onDestroy() {		 		 		 		 		 
		 super.onDestroy();	
	     mEngine.getScene().clearChildScene();	        
	     mEngine.getScene().clearTouchAreas();
	     mEngine.getScene().clearUpdateHandlers();	     
		 
	     for(byte i=0; i<3; i++){		       	        
			 if(bodies[i].isActive())			 
				 bodies[i].setActive(false);			 			 
		 }
	     
	     mWorld.dispose();
		 System.gc();		 		 
	 }	 
	 
	 
	 //---------------------------AL PRESIONAR BACK	 
	 
	 public void onBackPressed() {
		 System.exit(0);
	 }
}